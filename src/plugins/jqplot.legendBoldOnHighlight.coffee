#called in the scope of the plot target
handle_highlight = (e,series_index,point_index,neighbor,plot) ->
  $(this).find('tr.jqplot-table-legend').eq(series_index).addClass('jqplot-circuitlab-bold-legend')

#called in teh scope of the plot traget
handle_unhighlight = (e) ->
  $(this).find('tr.jqplot-table-legend.jqplot-circuitlab-bold-legend').removeClass('jqplot-circuitlab-bold-legend')

handle_post_init = (target,data,options) ->
  this.target.on('jqplotHighlighterHighlight',handle_highlight)
  this.target.on('jqplotHighlighterUnhighlight',handle_unhighlight)

$.jqplot.postInitHooks.push(handle_post_init)
