/**
 * jqPlot
 * Pure JavaScript plotting plugin using jQuery
 *
 * Version: @VERSION
 * Revision: @REVISION
 *
 * Copyright (c) 2009-2011 Chris Leonello
 * jqPlot is currently available for use in all personal or commercial projects 
 * under both the MIT (http://www.opensource.org/licenses/mit-license.php) and GPL 
 * version 2.0 (http://www.gnu.org/licenses/gpl-2.0.html) licenses. This means that you can 
 * choose the license that best suits your project and use it accordingly. 
 *
 * Although not required, the author would appreciate an email letting him 
 * know of any substantial use of jqPlot.  You can reach the author at: 
 * chris at jqplot dot com or see http://www.jqplot.com/info.php .
 *
 * If you are feeling kind and generous, consider supporting the project by
 * making a donation at: http://www.jqplot.com/donate.php .
 *
 * sprintf functions contained in jqplot.sprintf.js by Ash Searle:
 *
 *     version 2007.04.27
 *     author Ash Searle
 *     http://hexmen.com/blog/2007/03/printf-sprintf/
 *     http://hexmen.com/js/sprintf.js
 *     The author (Ash Searle) has placed this code in the public domain:
 *     "This code is unrestricted: you are free to use it however you like."
 * 
 */
(function($) {
  //$.jqplot.eventListenerHooks.push(['jqplotMouseMove', this.handleMove]);
  /**
   * Class: $.jqplot.hoverGuide
   * Plugin which will show vertical/horizontal guide lines on the plot where the cursor is.
   * 
   * To use this plugin, include the js
   * file in your source:
   * 
   * > <script type="text/javascript" src="plugins/jqplot.hoverGuide.js"></script>
   *
   * TODO: Explain available options and other notes
   *
   * NOTE: You must also include the highlighter plugin in the same plot,
   *       but set highlighter.show to be False
   * 
   * You may pass any options for drawing the guideline as part of:
   *    this.rendererOptions
   * which will be passed along to the shapeRenderer
   */
  $.jqplot.HoverGuide = function(options)
  {
    // Group: Properties
    //
    //prop: show
    // true to show the guideline.
    this.show = $.jqplot.config.enablePlugins;
    // prop: shapeRenderer
    // Renderer used to draw the guideline.
    this.shapeRenderer = new $.jqplot.ShapeRenderer();
    this.shapeRenderer.init(options.rendererOptions);
    
    $.extend(true, this, options);
  };
  
  var locations = ['nw', 'n', 'ne', 'e', 'se', 's', 'sw', 'w'];
  var locationIndicies = {'nw':0, 'n':1, 'ne':2, 'e':3, 'se':4, 's':5, 'sw':6, 'w':7};
  var oppositeLocations = ['se', 's', 'sw', 'w', 'nw', 'n', 'ne', 'e'];

  // called with scope of plot
  $.jqplot.HoverGuide.init = function (target, data, opts)
  {
    var options = opts || {};
    // add a hoverGuide attribute to the plot
    this.plugins.hoverGuide = new $.jqplot.HoverGuide(options.hoverGuide);
    this.eventListenerHooks.add('jqplotMouseMove', this.plugins.hoverGuide.handleMouseMove);
    this.eventListenerHooks.add('jqplotMouseLeave', this.plugins.hoverGuide.handleMouseLeave);
  };
  
  // called within scope of series
  $.jqplot.HoverGuide.parseOptions = function (defaults, options)
  {
    // Add additional options to the series here
  };
  
  // called within context of plot
  // create a canvas which we can draw on.
  // insert it before the eventCanvas, so eventCanvas will still capture events.
  $.jqplot.HoverGuide.postPlotDraw = function()
  {
    // Memory Leaks patch (copied from jqplot.highlighter
    if (this.plugins.hoverGuide && this.plugins.hoverGuide.hoverGuideCanvas)
    {
        this.plugins.hoverGuide.hoverGuideCanvas.resetCanvas();
        this.plugins.hoverGuide.hoverGuideCanvas = null;
    }
    this.plugins.hoverGuide.hoverGuideCanvas = new $.jqplot.GenericCanvas();
    
    this.eventCanvas._elem.before(this.plugins.hoverGuide.hoverGuideCanvas.createElement(this._gridPadding, 'jqplot-hoverguide-canvas', this._plotDimensions, this));
    this.plugins.hoverGuide.hoverGuideCanvas.setContext();
  };
 
  // wire-up hooks
  $.jqplot.preInitHooks.push($.jqplot.HoverGuide.init);
  $.jqplot.preParseSeriesOptionsHooks.push($.jqplot.HoverGuide.parseOptions);
  $.jqplot.postDrawHooks.push($.jqplot.HoverGuide.postPlotDraw);
  
  // draws guideline
  function drawLine(gridpos, plot, options)
  {
    var hg = plot.plugins.hoverGuide;
    var ctx = hg.hoverGuideCanvas._ctx;
    // draw guideline
    hg.shapeRenderer.draw(ctx, [[gridpos.x,0],[gridpos.x,ctx.canvas.height]], options);
    ctx = null;
  };

  // draws highlight marker
  function drawMarker(plot,neighbor)
  {
    var hl = plot.plugins.highlighter;
    var s = plot.series[neighbor.seriesIndex];
    var smr = s.markerRenderer;
    var mr = hl.markerRenderer;
    mr.style = smr.style;
    mr.lineWidth = smr.lineWidth + hl.lineWidthAdjust;
    mr.size = smr.size + hl.sizeAdjust;
    var rgba = $.jqplot.getColorComponents(smr.color);
    var newrgb = [rgba[0], rgba[1], rgba[2]];
    var alpha = (rgba[3] >= 0.6) ? rgba[3]*0.6 : rgba[3]*(2-rgba[3]);
    mr.color = 'rgba('+newrgb[0]+','+newrgb[1]+','+newrgb[2]+','+alpha+')';
    mr.init();
    mr.draw(s.gridData[neighbor.pointIndex][0], s.gridData[neighbor.pointIndex][1], hl.highlightCanvas._ctx);
  }

  function drawTooltip(plot,series,neighbor,moveToFront)
  {
    // neighbor looks like: {seriesIndex: i, pointIndex:j, gridData:p, data:s.data[j]}
    // gridData should be x,y pixel coords on the grid.
    // add the plot._gridPadding to that to get x,y in the target.
    var hl = plot.plugins.highlighter;
    var elem = hl._tooltipElems[neighbor.seriesIndex];
    var serieshl = series.highlighter || {};

    if (moveToFront && plot.seriesStack.length > 1) {
      // detach from DOM and re-insert on top of all other tooltip divs
      var topTooltip = $(elem).detach();
      $(plot.targetId).find('div.jqplot-highlighter-tooltip').filter(':last').after(topTooltip);
    }

    var opts = $.extend(true, {}, hl, serieshl);

    if (opts.useAxesFormatters) {
        var xf = series._xaxis._ticks[0].formatter;
        var yf = series._yaxis._ticks[0].formatter;
        var xfstr = series._xaxis._ticks[0].formatString;
        var yfstr = series._yaxis._ticks[0].formatString;
        var str;
        var xstr = xf(xfstr, neighbor.data[0]);
        var ystrs = [];
        for (var i=1; i<opts.yvalues+1; i++) {
          ystrs.push(yf(yfstr, neighbor.data[i]));
        }
        if (typeof opts.formatString === 'string') {
          switch (opts.tooltipAxes) {
            case 'both':
            case 'xy':
              ystrs.unshift(xstr);
              ystrs.unshift(opts.formatString);
              str = $.jqplot.sprintf.apply($.jqplot.sprintf, ystrs);
              break;
            case 'yx':
              ystrs.push(xstr);
              ystrs.unshift(opts.formatString);
              str = $.jqplot.sprintf.apply($.jqplot.sprintf, ystrs);
              break;
            case 'x':
              str = $.jqplot.sprintf.apply($.jqplot.sprintf, [opts.formatString, xstr]);
              break;
            case 'y':
              ystrs.unshift(opts.formatString);
              str = $.jqplot.sprintf.apply($.jqplot.sprintf, ystrs);
              break;
            default: // same as xy
              ystrs.unshift(xstr);
              ystrs.unshift(opts.formatString);
              str = $.jqplot.sprintf.apply($.jqplot.sprintf, ystrs);
              break;
          } 
        }
        else {
          switch (opts.tooltipAxes) {
            case 'both':
            case 'xy':
              str = xstr;
              for (var i=0; i<ystrs.length; i++) {
                str += opts.tooltipSeparator + ystrs[i];
              }
              break;
            case 'yx':
              str = '';
              for (var i=0; i<ystrs.length; i++) {
                str += ystrs[i] + opts.tooltipSeparator;
              }
              str += xstr;
              break;
            case 'x':
              str = xstr;
              break;
            case 'y':
              str = ystrs.join(opts.tooltipSeparator);
              break;
            default: // same as 'xy'
              str = xstr;
              for (var i=0; i<ystrs.length; i++) {
                str += opts.tooltipSeparator + ystrs[i];
              }
              break;
          }                
        }
    }
    else {
      var str;
      if (typeof opts.formatString ===  'string') {
        str = $.jqplot.sprintf.apply($.jqplot.sprintf, [opts.formatString].concat(neighbor.data));
      }
      else {
        if (opts.tooltipAxes == 'both' || opts.tooltipAxes == 'xy') {
          str = $.jqplot.sprintf(opts.tooltipFormatString, neighbor.data[0]) + opts.tooltipSeparator + $.jqplot.sprintf(opts.tooltipFormatString, neighbor.data[1]);
        }
        else if (opts.tooltipAxes == 'yx') {
          str = $.jqplot.sprintf(opts.tooltipFormatString, neighbor.data[1]) + opts.tooltipSeparator + $.jqplot.sprintf(opts.tooltipFormatString, neighbor.data[0]);
        }
        else if (opts.tooltipAxes == 'x') {
          str = $.jqplot.sprintf(opts.tooltipFormatString, neighbor.data[0]);
        }
        else if (opts.tooltipAxes == 'y') {
          str = $.jqplot.sprintf(opts.tooltipFormatString, neighbor.data[1]);
        } 
      }
    }
    if ($.isFunction(opts.tooltipContentEditor)) {
      // args str, seriesIndex, pointIndex are essential so the hook can look up
      // extra data for the point.
      str = opts.tooltipContentEditor(str, neighbor.seriesIndex, neighbor.pointIndex, plot);
    }
    elem.html(str);
    var gridpos = {x:neighbor.gridData[0], y:neighbor.gridData[1]};
    var ms = 0;
    var fact = 0.707;
    if (series.markerRenderer.show == true) { 
      ms = (series.markerRenderer.size + opts.sizeAdjust)/2;
    }

    var loc = locations;
    if (series.fillToZero && series.fill && neighbor.data[1] < 0) {
      loc = oppositeLocations;
    }

    switch (loc[locationIndicies[opts.tooltipLocation]]) {
      case 'nw':
        var x = gridpos.x + plot._gridPadding.left - elem.outerWidth(true) - opts.tooltipOffset - fact * ms;
        var y = gridpos.y + plot._gridPadding.top - opts.tooltipOffset - elem.outerHeight(true) - fact * ms;
        break;
      case 'n':
        var x = gridpos.x + plot._gridPadding.left - elem.outerWidth(true)/2;
        var y = gridpos.y + plot._gridPadding.top - opts.tooltipOffset - elem.outerHeight(true) - ms;
        break;
      case 'ne':
        var x = gridpos.x + plot._gridPadding.left + opts.tooltipOffset + fact * ms;
        var y = gridpos.y + plot._gridPadding.top - opts.tooltipOffset - elem.outerHeight(true) - fact * ms;
        break;
      case 'e':
        var x = gridpos.x + plot._gridPadding.left + opts.tooltipOffset + ms;
        var y = gridpos.y + plot._gridPadding.top - elem.outerHeight(true)/2;
        break;
      case 'se':
        var x = gridpos.x + plot._gridPadding.left + opts.tooltipOffset + fact * ms;
        var y = gridpos.y + plot._gridPadding.top + opts.tooltipOffset + fact * ms;
        break;
      case 's':
        var x = gridpos.x + plot._gridPadding.left - elem.outerWidth(true)/2;
        var y = gridpos.y + plot._gridPadding.top + opts.tooltipOffset + ms;
        break;
      case 'sw':
        var x = gridpos.x + plot._gridPadding.left - elem.outerWidth(true) - opts.tooltipOffset - fact * ms;
        var y = gridpos.y + plot._gridPadding.top + opts.tooltipOffset + fact * ms;
        break;
      case 'w':
        var x = gridpos.x + plot._gridPadding.left - elem.outerWidth(true) - opts.tooltipOffset - ms;
        var y = gridpos.y + plot._gridPadding.top - elem.outerHeight(true)/2;
        break;
      default: // same as 'nw'
        var x = gridpos.x + plot._gridPadding.left - elem.outerWidth(true) - opts.tooltipOffset - fact * ms;
        var y = gridpos.y + plot._gridPadding.top - opts.tooltipOffset - elem.outerHeight(true) - fact * ms;
        break;
    }
    elem.css('left', x);
    elem.css('top', y);
    elem.show();
    elem = null;  
  }

  // finds the closest point to the given position
  // NOTE: calculates distance using x-coord ONLY 
  function findNeighbors(gridpos, plot)
  {
    var neighbors = [];
    var series = plot.series;
    var x = gridpos.x, y = gridpos.y;
    for (var k = plot.seriesStack.length-1; k >= 0; k--)
    {
      var i = plot.seriesStack[k];
      var s = series[i];
      var d0 = null,i0,j0,p0;
      for (var j = 0; j < s.gridData.length; j++)
      {
        var p = s.gridData[j];
        if (p[0] != null && p[1] != null)
        {
          var d = Math.sqrt( (x-p[0]) * (x-p[0]) * 2.0 );
          if (d0 == null || d <= d0)
          {
            d0 = d; i0 = i; j0 = j; p0 = p;
          }
        }
      }
      if (d0 != null)
      {
        neighbors.push({seriesIndex: i0, pointIndex: j0, gridData: p0, data: s.data[j0]});
      }
    }
    return neighbors;
  }

  // called whenever there's a jqplotMouseMove event
  $.jqplot.HoverGuide.prototype.handleMouseMove = function(ev, gridpos, datapos, neighbor, plot)
  {
    //console.log('hoverGuide mouseMove: ',plot.targetId,neighbor);
    var hg = plot.plugins.hoverGuide;
    if (hg.show) 
    {
      // clear canvas      
      var ctx = hg.hoverGuideCanvas._ctx;
      ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
      // draw guideline
      drawLine(gridpos,plot,plot.rendererOptions);
    
      // clear highlighter canvas
      var hl,hl_ctx;
      if (plot.plugins.highlighter != null)
      {
        hl = plot.plugins.highlighter;
        hl_ctx = hl.highlightCanvas._ctx;
        hl_ctx.clearRect(0, 0, hl_ctx.canvas.width, hl_ctx.canvas.height);
        for(var i in hl._tooltipElems) {
          hl._tooltipElems[i].hide();
        }
      }
      hl_ctx = null
      
      // check if cursor is near a point on a plot
      if (neighbor != null)
      {
        // find neighboring points (by x-coord only)
        var neighbors = findNeighbors(gridpos,plot);
        for (var n = 0; n < neighbors.length; n++) {
          var nb = neighbors[n];
          // only draw marker and tooltip if we're showing that series
          // and if it is actually on the screen
          if (plot.series[nb.seriesIndex].show && nb.gridData[1] >= 0 && nb.gridData[1] <= ctx.canvas.height) 
          {
            drawMarker(plot,nb);
            if (nb.seriesIndex == neighbor.seriesIndex && plot.plugins.cursor.onGrid) {
              var evt1 = jQuery.Event('jqplotHighlighterUnhighlight');
              plot.target.trigger(evt1);
              
              var evt = jQuery.Event('jqplotHighlighterHighlight');
              evt.which = ev.which;
              evt.pageX = ev.pageX;
              evt.pageY = ev.pageY;
              var ins = [neighbor.seriesIndex, neighbor.pointIndex, neighbor.data, plot];
              plot.target.trigger(evt, ins);
              
              drawTooltip(plot,plot.series[nb.seriesIndex],nb,true);
            } else {
              drawTooltip(plot,plot.series[nb.seriesIndex],nb,false);
            }
          }
        }
        plot.moveSeriesToFront(neighbor.seriesIndex);
      }
      else 
      {
        var evt = jQuery.Event('jqplotHighlighterUnhighlight');
        plot.target.trigger(evt);
      }
      ctx = null;
    }
  };
  
  // called whenever there's a jqplotMouseLeave event
  $.jqplot.HoverGuide.prototype.handleMouseLeave = function(ev, gridpos, datapos, neighbor, plot)
  {
    var hg = plot.plugins.hoverGuide;
    if (hg.show) 
    {
      var ctx = hg.hoverGuideCanvas._ctx;
      ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
      ctx = null;
      var hl,hl_ctx;
      if (plot.plugins.highlighter != null)
      {
        hl = plot.plugins.highlighter;
        hl_ctx = hl.highlightCanvas._ctx;
        hl_ctx.clearRect(0, 0, hl_ctx.canvas.width, hl_ctx.canvas.height);
        for(var i in hl._tooltipElems) {
          hl._tooltipElems[i].hide();
        }
      }
      hl_ctx = null;
    }
  };
})(jQuery);
