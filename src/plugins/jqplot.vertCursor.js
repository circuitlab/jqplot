/**
 * jqPlot
 * Pure JavaScript plotting plugin using jQuery
 *
 * Version: @VERSION
 * Revision: @REVISION
 *
 * Copyright (c) 2009-2011 Chris Leonello
 * jqPlot is currently available for use in all personal or commercial projects 
 * under both the MIT (http://www.opensource.org/licenses/mit-license.php) and GPL 
 * version 2.0 (http://www.gnu.org/licenses/gpl-2.0.html) licenses. This means that you can 
 * choose the license that best suits your project and use it accordingly. 
 *
 * Although not required, the author would appreciate an email letting him 
 * know of any substantial use of jqPlot.  You can reach the author at: 
 * chris at jqplot dot com or see http://www.jqplot.com/info.php .
 *
 * If you are feeling kind and generous, consider supporting the project by
 * making a donation at: http://www.jqplot.com/donate.php .
 *
 * sprintf functions contained in jqplot.sprintf.js by Ash Searle:
 *
 *     version 2007.04.27
 *     author Ash Searle
 *     http://hexmen.com/blog/2007/03/printf-sprintf/
 *     http://hexmen.com/js/sprintf.js
 *     The author (Ash Searle) has placed this code in the public domain:
 *     "This code is unrestricted: you are free to use it however you like."
 * 
 */
(function($) {
  /**
   * Class: $.jqplot.vertCursor
   * Plugin which will show two draggable vertical "cursor" lines on the plot
   * 
   * To use this plugin, include the js
   * file in your source:
   * 
   * > <script type="text/javascript" src="plugins/jqplot.vertCursor.js"></script>
   *
   * TODO: Explain available options and other notes
   * 
   * You may pass any options for drawing the cursor line as part of:
   *    this.rendererOptions
   * which will be passed along to the shapeRenderer
   */
  $.jqplot.VertCursor = function(options)
  {
    // Group: Properties
    //
    //prop: show
    // true to show the cursor line.
    this.show = $.jqplot.config.enablePlugins;
    // prop: shapeRenderer
    // Renderer used to draw the cursor line.
    this.shapeRenderer = new $.jqplot.ShapeRenderer();
    this.shapeRenderer.init(options.rendererOptions);    
    // prop: props
    // these hold the sub-properties for each set of cursors
    this.props = {
      'xaxis':{},   // vertical lines marking positions along x-axis
      'yaxis':{}    // horizontal lines marking positions along y-axis
    };
    // prop: activeCursors
    // Either 0, 1 or 2
    // How many cursors are active and in use by the user
    this.props.xaxis.activeCursors = 0;
    this.props.yaxis.activeCursors = 0;
    // prop: _cursor
    // X position of the two cursor lines
    this.props.xaxis._cursors = [-1,-1];
    this.props.yaxis._cursors = [-1,-1];
    // prop: _cursorValues
    // x-axis values of the two cursor lines
    this.props.xaxis._cursorValues = [NaN,NaN];
    this.props.yaxis._cursorValues = [NaN,NaN];
    this.props.xaxis.cursors = [NaN,NaN];   //public property for options
    this.props.yaxis.cursors = [NaN,NaN];   //public property for options
    // prop: isDragging
    // true when either cursor line is being dragged by the user
    this.isDragging = false;
    // prop: draggedCursor
    // Which cursor line is currently being dragged by the user
    this.draggedCursor = null;
    // prop: loadedCursors
    // bool flag for whether the cursors have been loaded from options
    this.loadedCursors = false;
    // prop: _handleWidth, _handleHeight
    // The handles for each cursor line are 2*_handleWidth x 2*_handleHeight
    this._handleWidth = 4;
    this._handleHeight = 20;
    // prop: _initialOffset
    // How far from the edge of the plot to initially draw the cursor lines
    this._initialOffset = 10;
    // prop: _tooltipElem
    // Container for "info box" toltip element
    this._tooltipElem;
    this.tooltipOffset = 10;
    // prop: mathTraces
    // Holds settings for math functions
    this.mathTraces = {average:[],RMS:[],integral:[]};
    // prop: plotType
    // What type of plot this is, one of [default,time,dcsweep,bode]
    this.plotType = 'default';

    $.extend(true, this, options);
  };
  
  // called with scope of plot
  $.jqplot.VertCursor.preInit = function (target, data, opts)
  {
    var options = opts || {};
    // add a vertCursor attribute to the plot
    this.plugins.vertCursor = new $.jqplot.VertCursor(options.vertCursor);
    // setup event handlers
    this.eventListenerHooks.add('jqplotMouseMove', this.plugins.vertCursor.handleMouseMove);
    this.eventListenerHooks.add('jqplotMouseLeave', this.plugins.vertCursor.handleMouseLeave);
    this.eventListenerHooks.add('jqplotMouseDown', this.plugins.vertCursor.handleMouseDown);
    this.eventListenerHooks.add('jqplotMouseUp', this.plugins.vertCursor.handleMouseUp);
    // global hook will fire before the plot-bound ones above
    $.jqplot.eventListenerHooks.push(['jqplotMouseDown', this.plugins.vertCursor.handleGlobalMouseDown]);
    var plot = this;
    $(document).on('circuitlab.requestUpdateCursorToolTip',function(){
      var c = plot.plugins.vertCursor.props['xaxis'];
      if(c._cursors[0] == -1 && c._cursors[1] == -1){
        var width= plot.plugins.vertCursor.vertCursorCanvas._ctx.canvas.width;
        c._cursors[0] = 1;
        c._cursors[1] = width-1;
        c._cursorValues[0] = plot.axes.xaxis.series_p2u(c._cursors[0]);
        c._cursorValues[1] = plot.axes.xaxis.series_p2u(c._cursors[1]);
        c.activeCursors = 2;
      }
      updateTooltip(plot);  
    });
  };

  // called with scope of plot
  $.jqplot.VertCursor.postInit = function (target, data, opts)
  {
    this.target.on('jqplotZoom', this.plugins.vertCursor.handleZoom);
    this.target.on('jqplotResetZoom', this.plugins.vertCursor.handleResetZoom);
  }
  
  // called within scope of series
  $.jqplot.VertCursor.parseOptions = function (defaults, options)
  {
    // Add additional options to the series here
  };
  
  // called within context of plot
  // create a canvas which we can draw on.
  // insert it before the eventCanvas, so eventCanvas will still capture events.
  $.jqplot.VertCursor.postPlotDraw = function()
  {
    // Memory Leaks patch (copied from jqplot.highlighter
    if (this.plugins.vertCursor && this.plugins.vertCursor.vertCursorCanvas)
    {
      this.plugins.vertCursor.vertCursorCanvas.resetCanvas();
      this.plugins.vertCursor.vertCursorCanvas = null;
    }
    if (this.plugins.vertCursor._tooltipElem)
    {
      this.plugins.vertCursor._tooltipElem.emptyForce();
      this.plugins.vertCursor._tooltipElem = null;
    }
    var vc = this.plugins.vertCursor;
    vc.vertCursorCanvas = new $.jqplot.GenericCanvas();
    
    // Draw vertCursor canvas
    this.eventCanvas._elem.before(vc.vertCursorCanvas.createElement(this._gridPadding, 'jqplot-vertcursor-canvas', this._plotDimensions, this));
    vc.vertCursorCanvas.setContext();

    // Create tooltip div
    var elem = document.createElement('div');
    vc._tooltipElem = $(elem);
    elem = null;
    vc._tooltipElem.addClass('jqplot-vertcursor-tooltip');
    vc._tooltipElem.css({position:'absolute'});
    this.eventCanvas._elem.after(vc._tooltipElem);
    positionTooltip(this);
    
    // Initialize cursors and redraw
    if (!vc.loadedCursors) 
    {
      for (var dir in vc.props) 
      {
        var canvas_size = (dir == 'xaxis' ? vc.vertCursorCanvas._ctx.canvas.width : vc.vertCursorCanvas._ctx.canvas.height);
        for (var i = 0; i < vc.props[dir]._cursors.length; i++) 
        {
          if (vc.props[dir].cursors[i] == null)
            vc.props[dir].cursors[i] = NaN;
          var pos = this.axes[dir].series_u2p(vc.props[dir].cursors[i]);
          if (pos > 0 && pos < canvas_size) 
          {
            vc.props[dir]._cursors[i] = pos;
            vc.props[dir]._cursorValues[i] = vc.props[dir].cursors[i];
            vc.props[dir].activeCursors += 1;
          }
        }
      }
      vc.loadedCursors = true;
    }
    updateTooltip(this);
    drawLines(this, false, false, false, vc.rendererOptions);
  };
 
  // wire-up hooks
  $.jqplot.preInitHooks.push($.jqplot.VertCursor.preInit);
  $.jqplot.postInitHooks.push($.jqplot.VertCursor.postInit);
  $.jqplot.preParseSeriesOptionsHooks.push($.jqplot.VertCursor.parseOptions);
  $.jqplot.postDrawHooks.push($.jqplot.VertCursor.postPlotDraw);
    
  // position info tooltip on right side of graph
  function positionTooltip(plot) { 
    var grid = plot._gridPadding; 
    var vc = plot.plugins.vertCursor;
    var elem = vc._tooltipElem;  
    var x = grid.left + vc.tooltipOffset;
    var y = grid.bottom + vc.tooltipOffset;
    elem.css({'left': x, 'bottom': y});
    elem = null;
  }
  
  // format value using string formatter for the given axis
  function axisFormatter(plot,axis,val,customFormatString) {
    var axisTick = plot.axes[axis]._ticks[0];
    if(customFormatString == null || customFormatString == undefined){
      customFormatString = axisTick.formatString;
    } 
    return axisTick['formatter'](customFormatString,val);
  }

  function calculateAverage(ldataindex,rdataindex,series) {
    var s = series;
    var relevantData = s.data.slice(ldataindex,rdataindex);
    var integral = calculateIntegral(ldataindex,rdataindex,series);
    var delta_t = relevantData[relevantData.length-1][0]-relevantData[0][0];
    return integral/delta_t; 
  }

  function calculateRMS(ldataindex,rdataindex,series) {
    var s = series;
    var relevantData = s.data.slice(ldataindex,rdataindex);
    var totalSquared = calculateIntegral(ldataindex,rdataindex,series,function(a){return Math.pow(a,2);});
    var deltaT = relevantData[relevantData.length-1][0]-relevantData[0][0];
    return Math.sqrt(totalSquared/deltaT); 
  }
 
  function calculateIntegral(ldataindex,rdataindex,series,func) {
    //func is a function that is applied to each value before the integration happens
    //userful for RMS calcualtion. identity funciton by default
    if(func == null || func == undefined){
      func = function(a){return a;};
    }
    var s = series;
    var relevantData = s.data.slice(ldataindex,rdataindex);
    var integral= relevantData.reduce(function(a,b,i,array) {
      if(i==0){
        return 0;
      } else {
        return a + (array[i][0]-array[i-1][0])*(func(array[i][1])+func(array[i-1][1]))/2.0;
      }
    },0);
    return integral; 
  } 

  function getCursorUnitPosition(plot) {
    var xAxis = plot.axes['xaxis'];
    var unitPos1 = xAxis.series_p2u(vc.props['xaxis']._cursors[0]);
    var unitPos2 = xAxis.series_p2u(vc.props['xaxis']._cursors[1]);
    return {lunitpos:Math.min(unitPos1,unitPos2),runitpos:Math.max(unitPos1,unitPos2)};
  }

  function getCursorUnitIndex(plot) {
    var vc = plot.plugins.vertCursor;
    var index1 = findNeighbors({x:vc.props['xaxis']._cursors[0],y:0}, plot)[0].pointIndex;
    var index2 = findNeighbors({x:vc.props['xaxis']._cursors[1],y:0}, plot)[0].pointIndex;
    return {lindex:Math.min(index1,index2),rindex:Math.max(index1,index2)};
  }

  function generateToolTipMathDiv(plot,trace,mathFuncLabel){
    var dataIndexes = getCursorUnitIndex(plot);
    for (var k = 0; k < plot.series.length; k++) {
      var s = plot.series[k];
      var content = "";
      if (s.label == trace){
        var mathContainer = $("<div class='cursorLegend cursorLegend_" + trace + "'></div>");
        mathContainer.css('width','100%');
        mathContainer.css('clear','both');
        var text = $("<div class='cursorLegendText'></div>");
        text.css('float','left');
        var closeButtonWrapper = $("<div></div>").css({'height':'15px','width':'15px','float':'left'});
        var closeButton = $("<a href='#'><i class='icon-remove-sign'></i></a>");
        closeButtonWrapper.append(closeButton);
        mathContainer.append(createSwatch(s.color),text);
        //content += createSwatch(s.color)[0].outerHTML;
        if(mathFuncLabel == 'average'){
          content += "Average: ";
          content += axisFormatter(plot,s.yaxis,calculateAverage(dataIndexes.lindex,dataIndexes.rindex,s));
        } else if (mathFuncLabel == 'RMS'){
          content += "RMS: ";
          content += axisFormatter(plot,s.yaxis,calculateRMS(dataIndexes.lindex,dataIndexes.rindex,s));
        } else if (mathFuncLabel == 'integral'){
          content += "Integral: ";
          content += axisFormatter(plot,s.yaxis,calculateIntegral(dataIndexes.lindex,dataIndexes.rindex,s))+"*s";
        }
        text.html(content);
        mathContainer.append(closeButtonWrapper);
        closeButton.hide();
        mathContainer.on('mouseenter',function(){
          closeButton.show();
        });
        mathContainer.on('mouseleave',function(){
          closeButton.hide();
        });
        closeButton.on('click',function(e){
          var vc = plot.plugins.vertCursor;
          var traceIndex = vc.mathTraces.average.indexOf(trace);
          vc.mathTraces[mathFuncLabel].splice(traceIndex,1);
          updateTooltip(plot);
          e.preventDefault();
        });
        return mathContainer;
      }
    }
  }

  // update contents of info tooltip
  function updateTooltip(plot) {
    var vc = plot.plugins.vertCursor;
    var elem = vc._tooltipElem;
    elem.empty();
    var content;
    if (vc.show)
    {
      for (var dir in vc.props)
      {
        var c = vc.props[dir];
        var delta_line = $("<div class='cursorLegendText cursorLegend_delta"+dir[0]+"'></div>");
        elem.append(delta_line);
        var axis = plot.axes[dir];
        var ldata = axis.series_p2u(c._cursors[0]);
        var rdata = axis.series_p2u(c._cursors[1]);
        var label = dir == 'xaxis' ? 'X' : 'Y';
        if (vc.plotType == 'time' && dir == 'xaxis')
          label = 'T';
        if (c._cursors[0] > -1 && c._cursors[1] > -1) 
        {
          if(label == 'T'){
            var delta_t = Math.abs(rdata-ldata);
            var l = label+"1: "+axisFormatter(plot,dir,ldata)+"&nbsp;&nbsp;&nbsp;&nbsp;"+label+"2: "+axisFormatter(plot,dir,rdata)+"<br/>";
            l += "&#916;"+label+": "+axisFormatter(plot,dir,delta_t);
            if(delta_t != 0) {
              l += "&nbsp;&nbsp;&nbsp;&nbsp;&#916;1/T: "+axisFormatter(plot,dir,1.0/delta_t,'Hz');
            }
            delta_line.html(l);
          }
          else {
            delta_line.html("&#916;"+label+": "+axisFormatter(plot,dir,Math.abs(rdata-ldata))+"&nbsp;&nbsp;&nbsp;&nbsp;"+label+"1: "+axisFormatter(plot,dir,ldata)+"&nbsp;&nbsp;&nbsp;&nbsp;"+label+"2: "+axisFormatter(plot,dir,rdata));
          }
          if (dir == 'yaxis' && plot.axes['y2axis'].show) {
            //Add additional delta line in tooltip for second y-axis
            var y2_delta = $("<div class='cursorLegendText cursorLegend_deltay2'></div>");
            var data1 = plot.axes['y2axis'].series_p2u(c._cursors[0]);
            var data2 = plot.axes['y2axis'].series_p2u(c._cursors[1]);
            y2_delta.html("&#916;"+label+": "+axisFormatter(plot,'y2axis',Math.abs(data1-data2))+"&nbsp;&nbsp;&nbsp;&nbsp;"+label+"1: "+axisFormatter(plot,'y2axis',data1)+"&nbsp;&nbsp;&nbsp;&nbsp;"+label+"2: "+axisFormatter(plot,'y2axis',data2));
            elem.append(y2_delta);
          }
        } 
        else if (c._cursors[0] > -1 || c._cursors[1] > -1) 
        {
          var content = "";
          content += "&#916;"+label+": &#8212;";
          if (c._cursors[0] > -1)
            content += "&nbsp;&nbsp;&nbsp;&nbsp;"+label+"1: "+axisFormatter(plot,dir,ldata);
          else
            content += "&nbsp;&nbsp;&nbsp;&nbsp;"+label+"1: &#8212;";
          if (c._cursors[1] > -1)
            content += "&nbsp;&nbsp;&nbsp;&nbsp;"+label+"2: "+axisFormatter(plot,dir,rdata)+"<br />";
          else
            content += "&nbsp;&nbsp;&nbsp;&nbsp;"+label+"2: &#8212;<br />";
          delta_line.html(content);
        } 
      }
      //Iterate through the traces in the math functions we want
      if (vc.plotType == 'time' && vc.props.xaxis._cursors[0] > -1 && vc.props.xaxis._cursors[1] > -1){
        //Iterate through the traces in the match functions we want
        $.each(vc.mathTraces, function(mathFunc,traces){
          for(var i=0;i<traces.length;i++){
            elem.append(generateToolTipMathDiv(plot,traces[i],mathFunc));
          }
          if(traces.length>0){
            elem.append($("<div></div>").css('clear','both'));
          }
        });
      }
      elem.hide();
      elem.children('div').each(function(){
        if(!$(this).is(':empty')){
          elem.show();
          return false;
        }
      });
    }
    else
    {
      elem.hide();
    }
    elem = null;
  }
  
  function createSwatch(color) {
    var swatch = $("<div class='jqplot-table-legend-swatch' style='float:left;margin-top:0.35em;margin-right:2px;'></div>");
    swatch.css({'background-color':color,'border-color':color});
    return swatch;
  }

  // finds the closest point to the given position
  // NOTE: calculates distance using x-coord ONLY 
  function findNeighbors(gridpos, plot)
  {
    var neighbors = {};
    var series = plot.series;
    var x = gridpos.x, y = gridpos.y;
    for (var k = plot.seriesStack.length-1; k >= 0; k--)
    {
      var i = plot.seriesStack[k];
      var s = series[i];
      var d0 = null,i0,j0,p0;
      for (var j = 0; j < s.gridData.length; j++)
      {
        var p = s.gridData[j];
        if (p[0] != null && p[1] != null)
        {
          var d = Math.sqrt( (x-p[0]) * (x-p[0]) * 2.0 );
          if (d0 == null || d <= d0)
          {
            d0 = d; i0 = i; j0 = j; p0 = p;
          }
        }
      }
      if (d0 != null)
      {
        neighbors[i0] = {seriesIndex: i0, pointIndex: j0, gridData: p0, data: s.data[j0]};
      }
    }
    return neighbors;
  }

  // check if mouse is over cursor handles
  function checkHandleBoundingBox(plot,pos) {
    var vc = plot.plugins.vertCursor;
    // check vertical cursors
    var c = vc.props['xaxis'];
    var center = vc.vertCursorCanvas._ctx.canvas.height / 2;
    for (var i = 0; i < c._cursors.length; i++)
      if (c._cursors[i] > -1 && pos.x >= c._cursors[i] - vc._handleWidth && pos.x <= c._cursors[i] + vc._handleWidth)
          //&& pos.y >= center - vc._handleHeight && pos.y <= center + vc._handleHeight)
        return ['xaxis',i];
    // check horizontal cursors 
    c = vc.props['yaxis'];
    center = vc.vertCursorCanvas._ctx.canvas.width / 2;
    for (var i = 0; i < c._cursors.length; i++)
      if (c._cursors[i] > -1 && pos.y >= c._cursors[i] - vc._handleWidth && pos.y <= c._cursors[i] + vc._handleWidth)
          //&& pos.x >= center - vc._handleHeight && pos.x <= center + vc._handleHeight)
        return ['yaxis',i];
    return null;
  }
  
  // check if mouse is over far-left or far-right side
  // of the plot where cursors can be dragged from
  function checkSideBarBoundingBox(plot,pos) {
    var vc = plot.plugins.vertCursor;
    return (pos.x <= vc._initialOffset+vc._handleWidth || pos.x >= vc.vertCursorCanvas._ctx.canvas.width-vc._initialOffset-vc._handleWidth);
  }
    
  // check if mouse is over very top or bottom
  // of the plot where cursors can be dragged from
  function checkTopBotBarBoundingBox(plot,pos) {
    var vc = plot.plugins.vertCursor;
    return (pos.y <= vc._initialOffset+vc._handleWidth || pos.y >= vc.vertCursorCanvas._ctx.canvas.height-vc._initialOffset-vc._handleWidth);
  }
  
  // number of active cursors that are off-screen due to zoom
  function getNumOffScreenCursors(plot, dir) {
    var n = 0;
    var vc = plot.plugins.vertCursor;
    var screen_size = (dir == 'xaxis' ? vc.vertCursorCanvas._ctx.canvas.width : vc.vertCursorCanvas._ctx.canvas.height);
    for (var i = 0; i < vc.props[dir]._cursors.length; i++) {
      var new_pos = plot.axes[dir].series_u2p(vc.props[dir]._cursorValues[i]);
      if (!isNaN(vc.props[dir]._cursorValues[i]) && (new_pos <= 0 || new_pos >= screen_size))
        n++;
    }
    return n;
  }
  
  // draws cursor lines
  function drawLines(plot, withHandles, withSideBars, withTopBotBars, options)
  {
    var vc = plot.plugins.vertCursor;
    if (vc.show)
    {
      var ctx = vc.vertCursorCanvas._ctx;
      var center_height = ctx.canvas.height / 2;
      var center_width = ctx.canvas.width / 2;
      // shapeRenderer options for handles
      var opts = {
        'fill':true,
        'closePath':true,
        'strokeStyle':options.strokeStyle,
        'fillStyle':options.strokeStyle
      };
      for (var dir in vc.props) {
        for (var i = 0; i < vc.props[dir]._cursors.length; i++) {
          var pos = vc.props[dir]._cursors[i];
          // draw cursor lines
          if (pos > -1 && dir == 'xaxis') {
            vc.shapeRenderer.draw(ctx, [[pos,0],[pos,ctx.canvas.height]], options);
          } else if (pos > -1 && dir == 'yaxis') {
            vc.shapeRenderer.draw(ctx, [[0,pos],[ctx.canvas.width,pos]], options);
          }
          // draw handles
          if (withHandles && pos > -1 && dir == 'xaxis') {
            vc.shapeRenderer.draw(ctx, 
             [[pos-vc._handleWidth,center_height-vc._handleHeight],
              [pos+vc._handleWidth,center_height-vc._handleHeight],
              [pos+vc._handleWidth,center_height+vc._handleHeight],
              [pos-vc._handleWidth,center_height+vc._handleHeight]], opts);
          }
          else if (withHandles && pos > -1 && dir == 'yaxis') {
            vc.shapeRenderer.draw(ctx, 
             [[center_width-vc._handleHeight,pos-vc._handleWidth],
              [center_width-vc._handleHeight,pos+vc._handleWidth],
              [center_width+vc._handleHeight,pos+vc._handleWidth],
              [center_width+vc._handleHeight,pos-vc._handleWidth]], opts);
          }
        }
      }
      // draw draggable cursors on side bar
      if (withSideBars) {
        var left = vc._initialOffset;
        var right = ctx.canvas.width - vc._initialOffset;
        vc.shapeRenderer.draw(ctx, [[left,0],[left,ctx.canvas.height]], options);
        vc.shapeRenderer.draw(ctx, [[right,0],[right,ctx.canvas.height]], options);
        vc.shapeRenderer.draw(ctx, 
         [[left-vc._handleWidth,center_height-vc._handleHeight],
          [left+vc._handleWidth,center_height-vc._handleHeight],
          [left+vc._handleWidth,center_height+vc._handleHeight],
          [left-vc._handleWidth,center_height+vc._handleHeight]], opts);
        vc.shapeRenderer.draw(ctx, 
         [[right-vc._handleWidth,center_height-vc._handleHeight],
          [right+vc._handleWidth,center_height-vc._handleHeight],
          [right+vc._handleWidth,center_height+vc._handleHeight],
          [right-vc._handleWidth,center_height+vc._handleHeight]], opts);
      }
      // draw draggable cursors on top-and-bottom
      if (withTopBotBars) {
        var top = vc._initialOffset;
        var bot = ctx.canvas.height - vc._initialOffset;
        vc.shapeRenderer.draw(ctx, [[0,top],[ctx.canvas.width,top]], options);
        vc.shapeRenderer.draw(ctx, [[0,bot],[ctx.canvas.width,bot]], options);
        vc.shapeRenderer.draw(ctx, 
         [[center_width-vc._handleHeight,top-vc._handleWidth],
          [center_width-vc._handleHeight,top+vc._handleWidth],
          [center_width+vc._handleHeight,top+vc._handleWidth],
          [center_width+vc._handleHeight,top-vc._handleWidth]], opts);
        vc.shapeRenderer.draw(ctx, 
         [[center_width-vc._handleHeight,bot-vc._handleWidth],
          [center_width-vc._handleHeight,bot+vc._handleWidth],
          [center_width+vc._handleHeight,bot+vc._handleWidth],
          [center_width+vc._handleHeight,bot-vc._handleWidth]], opts);
      }
      ctx = null;
    }
  };
  
  // redraw cursor lines
  $.jqplot.VertCursor.prototype.redraw = function(plot)
  {
    var vc = plot.plugins.vertCursor;
    var ctx = vc.vertCursorCanvas._ctx;
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    drawLines(plot, false, false, false, vc.rendererOptions);
    updateTooltip(plot);
    ctx = null;
  }

  // called whenever there's a jqplotMouseMove event
  $.jqplot.VertCursor.prototype.handleMouseMove = function(ev, gridpos, datapos, neighbor, plot)
  {
    //console.log('mouse move: ',plot.targetId, '  at ',gridpos.x,' , ',gridpos.y);
    var vc = plot.plugins.vertCursor;
    if (vc.show) 
    {
      var ctx = vc.vertCursorCanvas._ctx;
      if (vc.isDragging)
      {
        if (vc.draggedCursor != null && vc.draggedCursor[0] == 'xaxis') {
          vc.props['xaxis']._cursors[vc.draggedCursor[1]] = gridpos.x;
          vc.props['xaxis']._cursorValues[vc.draggedCursor[1]] = plot.axes['xaxis'].series_p2u(gridpos.x);
        }
        else if (vc.draggedCursor != null && vc.draggedCursor[0] == 'yaxis') {
          vc.props['yaxis']._cursors[vc.draggedCursor[1]] = gridpos.y;
          vc.props['yaxis']._cursorValues[vc.draggedCursor[1]] = plot.axes['yaxis'].series_p2u(gridpos.y);
        }
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        drawLines(plot, true, false, false, vc.rendererOptions);
        updateTooltip(plot);
        //ev.stopImmediatePropagation();
      }
      else 
      {
        // is not currently dragging cursor, see if we should show/hide handles
        var showHandles = false, showSideBars = false, showTopBotBars = false;
        if (checkHandleBoundingBox(plot,gridpos) != null) {
          showHandles = true;
        }
        if (vc.props['xaxis'].activeCursors < 2 && checkSideBarBoundingBox(plot,gridpos)) {
          showSideBars = true;
        }
        if (vc.props['yaxis'].activeCursors < 2 && checkTopBotBarBoundingBox(plot,gridpos)) {
          showTopBotBars = true;
        }
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        drawLines(plot, showHandles, showSideBars, showTopBotBars, vc.rendererOptions);
      }
      ctx = null;
    }
  };
 
  // called whenever there's a jqplotMouseDown event
  $.jqplot.VertCursor.prototype.handleGlobalMouseDown = function(ev, gridpos, datapos, neighbor, plot)
  {
    var vc = plot.plugins.vertCursor;
    if (vc.show) 
    {
      // tell cursor.js to ignore mouseDown events so we don't
      // zoom when dragging the cursors around
      if (checkHandleBoundingBox(plot,gridpos) != null || 
         (checkSideBarBoundingBox(plot,gridpos) && vc.props['xaxis'].activeCursors < 2) ||
         (checkTopBotBarBoundingBox(plot,gridpos) && vc.props['yaxis'].activeCursors < 2)) {
        plot.plugins.cursor.ignoreMouseDown = true;
      } 
    }
  }

  // called whenever there's a jqplotMouseDown event
  $.jqplot.VertCursor.prototype.handleMouseDown = function(ev, gridpos, datapos, neighbor, plot)
  {
    //console.log('vertCursor mouseDown: ',plot.targetId,'  at ',gridpos.x,' , ',gridpos.y);
    var vc = plot.plugins.vertCursor;
    var isThisPlot = plot.plugins.cursor.onGrid;
    if (vc.show) 
    {
      var selectedCursor = checkHandleBoundingBox(plot,gridpos);
      if (selectedCursor != null) {
        // clicked on existing cursor
        if (selectedCursor[0] == 'xaxis' || isThisPlot) {
          vc.draggedCursor = selectedCursor;
          vc.isDragging = true;
        }
      } 
      // clicked on side bars to move off-screen cursor
      else if (checkSideBarBoundingBox(plot,gridpos) && getNumOffScreenCursors(plot,'xaxis') > 0) {
        var dir = 'xaxis';
        var c = vc.props[dir];
        var n = getNumOffScreenCursors(plot, dir);
        if (n == 1 && c.activeCursors == 0) {
          // one cursor total, and it's off-screen
          // create new cursor
          vc.draggedCursor = [dir,(isNaN(c._cursorValues[1]) ? 1 : 0)];
        }
        else if (n == 1 && c.activeCursors == 1) {
          // two cursors total, one is off-screen
          // select off-screen cursor
          vc.draggedCursor = [dir,(c._cursors[0] > -1 ? 1 : 0)];
        }
        else {
          var xpos1 = plot.axes[dir].series_u2p(c._cursorValues[0]);
          var xpos2 = plot.axes[dir].series_u2p(c._cursorValues[1]);
          if (xpos1*xpos2 > 0) {
            // two off-screen cursors, on the same side
            // select the closest one
            vc.draggedCursor = [dir,(Math.abs(gridpos.x-xpos1) > Math.abs(gridpos.x-xpos2) ? 1 : 0)];
          }
          else {
            // two off-screen cursors, on different sides
            // select the one on the same side as the click
            vc.draggedCursor = [dir,(xpos1*(gridpos.x-(vc.vertCursorCanvas._ctx.canvas.width/2)) > 0 ? 0 : 1)];
          }
        }
        c.activeCursors += 1;
        vc.isDragging = true;
      } 
      // clicked on top/bottom bars to move off-screen cursor
      else if (checkTopBotBarBoundingBox(plot,gridpos) && getNumOffScreenCursors(plot,'yaxis') > 0 && isThisPlot) {
        var dir = 'yaxis';
        var c = vc.props[dir];
        var n = getNumOffScreenCursors(plot, dir);
        if (n == 1 && c.activeCursors == 0) {
          // one cursor total, and it's off-screen
          // create new cursor
          vc.draggedCursor = [dir,(isNaN(c._cursorValues[1]) ? 1 : 0)];
        }
        else if (n == 1 && c.activeCursors == 1) {
          // two cursors total, one is off-screen
          // select off-screen cursor
          vc.draggedCursor = [dir,(c._cursors[0] > -1 ? 1 : 0)];
        }
        else {
          var xpos1 = plot.axes[dir].series_u2p(c._cursorValues[0]);
          var xpos2 = plot.axes[dir].series_u2p(c._cursorValues[1]);
          if (xpos1*xpos2 > 0) {
            // two off-screen cursors, on the same side
            // select the closest one
            vc.draggedCursor = [dir,(Math.abs(gridpos.y-xpos1) > Math.abs(gridpos.y-xpos2) ? 1 : 0)];
          }
          else {
            // two off-screen cursors, on different sides
            // select the one on the same side as the click
            vc.draggedCursor = [dir,(xpos1*(gridpos.y-(vc.vertCursorCanvas._ctx.canvas.height/2)) > 0 ? 0 : 1)];
          }
        }
        c.activeCursors += 1;
        vc.isDragging = true;
      } 
      // clicked on side bars to create new cursor
      else if (checkSideBarBoundingBox(plot,gridpos) && vc.props['xaxis'].activeCursors < 2) {
        vc.draggedCursor = ['xaxis',(vc.props['xaxis']._cursors[0] > -1 ? 1 : 0)];
        vc.props['xaxis'].activeCursors += 1;
        vc.isDragging = true;
      }
      // clicked on top-and-bottom bars to create new cursor
      else if (checkTopBotBarBoundingBox(plot,gridpos) && vc.props['yaxis'].activeCursors < 2 && isThisPlot) {
        vc.draggedCursor = ['yaxis',(vc.props['yaxis']._cursors[0] > -1 ? 1 : 0)];
        vc.props['yaxis'].activeCursors += 1;
        vc.isDragging = true;
      }
    }
  };

  // called whenever there's a jqplotMouseUp event
  $.jqplot.VertCursor.prototype.handleMouseUp = function(ev, gridpos, datapos, neighbor, plot)
  {
    var vc = plot.plugins.vertCursor;
    if (vc.show) 
    {
      if (vc.isDragging) {
        vc.isDragging = false;
        vc.draggedCursor = null;
      }
    }
  };
  
  // called whenever there's a jqplotMouseLeave event
  $.jqplot.VertCursor.prototype.handleMouseLeave = function(ev, gridpos, datapos, neighbor, plot)
  {
    var vc = plot.plugins.vertCursor;
    if (vc.show) 
    {
      if (vc.isDragging) {
        // delete cursor when dragged off the plot
        var c = vc.props[vc.draggedCursor[0]];
        vc.isDragging = false;
        c._cursors[vc.draggedCursor[1]] = -1;
        c._cursorValues[vc.draggedCursor[1]] = NaN;
        c.activeCursors -= 1;
        vc.draggedCursor = null;
      }
      vc.redraw(plot);
    }
  };
 
  // called whenever there's a jqplotZoom event
  $.jqplot.VertCursor.prototype.handleZoom = function(ev, gridpos, datapos, plot, cursor)
  {
    var vc = plot.plugins.vertCursor;
    var ctx = vc.vertCursorCanvas._ctx;
    for (var dir in vc.props) {
      // calculate new cursor positions in zoomed view
      var c = vc.props[dir];
      var canvas_size = (dir == 'xaxis' ? ctx.canvas.width : ctx.canvas.height);
      c.activeCursors = 0;
      for (var i = 0; i < c._cursors.length; i++) {
        var new_pos = plot.axes[dir].series_u2p(c._cursorValues[i]);
        if (new_pos > 0 && new_pos < canvas_size) {
          // cursor stays within view
          c._cursors[i] = new_pos;
          c.activeCursors += 1;
        }
        else {
          // cursor is off-screen
          c._cursors[i] = -1;
        }
      }
    }
    if (vc.show) 
    {
      vc.redraw(plot);
    }
    ctx = null;
  };
  
  // called whenever there's a jqplotResetZoom event
  $.jqplot.VertCursor.prototype.handleResetZoom = function(ev, plot, cursor)
  {
    var vc = plot.plugins.vertCursor;
    for (var dir in vc.props) { 
      var c = vc.props[dir];
      c.activeCursors = 0;
      for (var i = 0; i < c._cursors.length; i++) {
        if (!isNaN(c._cursorValues[i])) {
          c._cursors[i] = plot.axes[dir].series_u2p(c._cursorValues[i]);
          c.activeCursors += 1;
        }
      }
    }
    if (vc.show) 
    {
      vc.redraw(plot);
    }
  };

  // show/hide the cursor lines and tooltip
  $.jqplot.VertCursor.prototype.setCursorShow = function(plot,show_flag)
  {
    this.show = show_flag;
    this.redraw(plot);
  };

  // returns cursor values
  $.jqplot.VertCursor.prototype.getCursors = function(plot)
  {
    return {'xaxis':this.props['xaxis']._cursorValues, 'yaxis':this.props['yaxis']._cursorValues};
  };

})(jQuery);
