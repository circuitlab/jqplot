all:	build/jqplot.js build/jqplot.css

build-dir:
	mkdir -p build

build/.closure-start:
	echo '(function($$) {' > build/.closure-start

build/.closure-end:
	echo '})(jQuery);' > build/.closure-end

build/.newline:
	echo "" > build/.newline

build/plugins_concat.js: build-dir
	#concat all the js plugin files together
	(cd src && cat Buildlist_plugins_js | grep -v '^#' | grep -v '^$$' | xargs -n 1 -I{} cat {} > ../build/.jqplot_plugins_js_concat)
	#concat all the coffeescript plugins together
	(cd src && cat Buildlist_plugins_coffee | grep -v '^#' | grep -v '^$$' | xargs -n 1 -I{} cat {} > ../build/.jqplot_plugins_coffee_concat)
	#compile the coffeescript plugins
	(cd build && coffee -c -b .jqplot_plugins_coffee_concat)
	#cat all the plugins together
	cat build/.jqplot_plugins_js_concat build/.jqplot_plugins_coffee_concat.js > build/plugins_concat.js

build/jquery.jqplot.js: build-dir build/.closure-start build/.closure-end build/.newline
	#cat the Buildlist together, remove individual closure for each file and wrap the whole thing in a single jQuery closure
	(cd src && cat Buildlist_core | xargs -n 1 -I{} cat ../build/.newline {} ../build/.newline | grep -v '^ *(function($$) { *$$' | grep -v '^ *})(jQuery); *$$' > ../build/.jqplot_concat.js)
	cat build/.closure-start build/.jqplot_concat.js build/.closure-end > build/jquery.jqplot.js

build/jqplot.js: build-dir build/jquery.jqplot.js build/plugins_concat.js
	cat build/jquery.jqplot.js build/plugins_concat.js > build/jqplot.js


build/jqplot.css:
	cp src/jquery.jqplot.css build/jqplot.css

clean:
	rm -rf build

.PHONY: build-dir	build/jquery.jqplot.js build/plugins_concat.js build/jqplot.js build/jqplot.css
